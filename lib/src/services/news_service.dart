import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:notice_provider/src/models/category_model.dart';
import 'package:notice_provider/src/models/news_models.dart';
import 'package:http/http.dart' as http;

final _URL_NEWS = 'https://newsapi.org/v2';
final _API_KEY = 'cbe6ce82c31c42b1804d3c472a8bb6ce';

class NewsService with ChangeNotifier {
  String _selectedCategory = 'business';
  List<Article> headline = [];
  List<Category> category = [
    Category(FontAwesomeIcons.building, 'business'),
    Category(FontAwesomeIcons.tv, 'entertainment'),
    Category(FontAwesomeIcons.addressCard, 'general'),
    Category(FontAwesomeIcons.headSideVirus, 'health'),
    Category(FontAwesomeIcons.vials, 'science'),
    Category(FontAwesomeIcons.baseballBall, 'sports'),
    Category(FontAwesomeIcons.memory, 'technology'),
  ];

  Map<String, List<Article>> categoryArticulos = {};

  NewsService() {
    this.getTopHeadlineas();
    category.forEach((item){
      categoryArticulos[item.name] = List();
    });
  }

  getTopHeadlineas() async {
    final url = '$_URL_NEWS/top-headlines?apiKey=$_API_KEY&country=us';

    final response = await http.get(url);

    final newsResponse = newsResponseFromJson(response.body);

    this.headline.addAll(newsResponse.articles);
    notifyListeners();
  }

  getCategoriasSeleccionadas(String category) async {
    if(this.categoryArticulos[category].length >0){
      return this.categoryArticulos[category];
    }
    final url =
        '$_URL_NEWS/top-headlines?apiKey=$_API_KEY&country=us&category=${category}';

    final response = await http.get(url);

    final newsResponse = newsResponseFromJson(response.body);

    this.categoryArticulos[category].addAll(newsResponse.articles);
    notifyListeners();
  }

  List<Article> get getArticulosCategoriasSeleccionada => this.categoryArticulos[this.selectedCategory];

  get selectedCategory => this._selectedCategory;
  set selectedCategory(String valor) {
    this._selectedCategory = valor;

    getCategoriasSeleccionadas(valor);

    notifyListeners();
  }
}
