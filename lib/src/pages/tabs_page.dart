import 'package:flutter/material.dart';
import 'package:notice_provider/src/pages/tab1_page.dart';
import 'package:notice_provider/src/pages/tab2_page.dart';
import 'package:provider/provider.dart';

class Tabspage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
          create: (_) => _NavegacionModel(),
          child: Scaffold(
        body: _Paginas(),
        bottomNavigationBar: _Navegacion(),
      ),
    );
  }
}

class _Navegacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);
    return BottomNavigationBar(
      currentIndex: navegacionModel.paginaActual,
      onTap: (i)=> navegacionModel.paginaActual = i,
      items: [
      BottomNavigationBarItem(
          icon: Icon(Icons.person_outline), title: Text('Para ti')),
      BottomNavigationBarItem(
          icon: Icon(Icons.whatshot), title: Text('Encabezado')),
    ]);
  }
}

class _Paginas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModel>(context);
    return PageView(
      controller: navegacionModel._pageController ,
      //physics: BouncingScrollPhysics(),
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        Tab1Page(),
        Tab2Page(),
      ],
    );
  }
}


class _NavegacionModel with ChangeNotifier{
  int _paginaAcual = 0;
  PageController _pageController = PageController();

  int get paginaActual => this._paginaAcual;

  set paginaActual(int valor){
    this._paginaAcual = valor;
    _pageController.animateToPage(valor, duration: Duration(milliseconds: 250), curve: Curves.bounceOut);


    notifyListeners();
  }

  PageController get pageController => this._pageController;

}