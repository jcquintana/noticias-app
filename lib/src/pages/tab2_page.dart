import 'package:flutter/material.dart';
import 'package:notice_provider/src/models/category_model.dart';
import 'package:notice_provider/src/services/news_service.dart';
import 'package:notice_provider/src/theme/theme.dart';
import 'package:notice_provider/src/widgets/lista_noticias.dart';
import 'package:provider/provider.dart';

class Tab2Page extends StatelessWidget {
  const Tab2Page({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        final newsService = Provider.of<NewsService>(context);
 
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            _Listacategoria(),
            Expanded(child: ListaNoticias(newsService.getArticulosCategoriasSeleccionada))
          ],
        ),
      ),
    );
  }
}

class _Listacategoria extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final categorias = Provider.of<NewsService>(context).category;
    return Container(
      width: double.infinity,
      height: 80,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: categorias.length,
        itemBuilder: (BuildContext context, int index) {
          final nameCat = categorias[index].name;
          return Container(
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  _CategoryButton(categorias[index]),
                  SizedBox(height: 5),
                  Text('${nameCat[0].toUpperCase()}${nameCat.substring(1)}')
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class _CategoryButton extends StatelessWidget {
  final Category categoria;

  const _CategoryButton(this.categoria);

  @override
  Widget build(BuildContext context) {
    final newsService = Provider.of<NewsService>(context);
    return GestureDetector(
      onTap: () {
        print('Hola mundo');
        newsService.selectedCategory = categoria.name;
      },
      child: Container(
        width: 40,
        height: 40,
        margin: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
        ),
        child: Icon(categoria.icon,
            color: (newsService.selectedCategory == categoria.name)
                ? tema.accentColor
                : Colors.black54),
      ),
    );
  }
}
