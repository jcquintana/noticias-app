import 'package:flutter/material.dart';
import 'package:notice_provider/src/pages/tabs_page.dart';
import 'package:notice_provider/src/services/news_service.dart';
import 'package:notice_provider/src/theme/theme.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
          providers: <SingleChildWidget>[
            ChangeNotifierProvider(create: (_)=> new NewsService()),
          ],
          child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: tema,
        home: Tabspage(),
      ),
    );
  }
}
